/*
 a * FieldVisualPlugin.h
 *
 *  Created on: Mar 31, 2016
 *      Author: Matthias Dingwerth
 */

#ifndef INCLUDE_FIELDGROUNDPLUGIN_H_
#define INCLUDE_FIELDGROUNDPLUGIN_H_

#include "boost/make_shared.hpp" 
#include "boost/shared_ptr.hpp" 
#include "gazebo/rendering/ogre_gazebo.h" 
#include "gazebo/gazebo.hh" 
#include "gazebo/common/common.hh" 
#include "gazebo/msgs/msgs.hh" 
#include "gazebo/transport/transport.hh" 
#include "gazebo/rendering/rendering.hh" 
 
#include "ros/ros.h" 
#include "ros/package.h" 
 
#include <opencv2/core/core.hpp> 
#include <opencv2/opencv.hpp> 
 
#include "string" 
#include <sdf/sdf.hh>

namespace gazebo {

/// \class FieldGroundPlugin FieldGroundPlugin.hh gazebo_world/FieldGroundPlugin.hh
/// \brief Ground of the field for the simulation
class FieldGroundPlugin: public VisualPlugin {
public:

	/// \brief constructor of this class [empty]
	FieldGroundPlugin();

	/// \brief destructor of this class [empty]
	virtual ~FieldGroundPlugin();

	/// \brief The fuction Load will run after the plugin is called. This function
	/// read the parameter of the sdf file and load the map from the storage.
	/// \param[in] _parent Pointer to the parent visual element
	/// \param[in] _sdf SDF Sensor parameters
	void Load(rendering::VisualPtr _parent, sdf::ElementPtr _sdf);



private:

	/// \brief Visual Pointer of the parent visual
	rendering::VisualPtr parent;

	/// \brief Element Pointer with the sdf values
	sdf::ElementPtr sdf;

};
}
;
#endif /* INCLUDE_FIELDGROUNDPLUGIN_H_ */
