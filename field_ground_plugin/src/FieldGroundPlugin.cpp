/*
 * GridVisualPlugin.cpp
 *
 *  Created on: Mar 31, 2016
 *      Author: Matthias Dingwerth
 */

#include "field_ground_plugin/FieldGroundPlugin.h"

namespace gazebo {

// Register this plugin with the simulator
GZ_REGISTER_VISUAL_PLUGIN(FieldGroundPlugin);

FieldGroundPlugin::FieldGroundPlugin() {

}

FieldGroundPlugin::~FieldGroundPlugin() {

}

void FieldGroundPlugin::Load(rendering::VisualPtr _parent,
		sdf::ElementPtr _sdf) {

	//
	std::string map_path;

	//Get the VisualPtr
	this->parent = _parent;


	// Get the sdf
	this->sdf = _sdf->GetElement("sdf");


	// Get the Mode of the field for the visualization
	if (!this->sdf->HasElement("path")) {
		map_path = {};
		ROS_ERROR("teset");
	} else {
		ROS_ERROR("teset1");
	map_path = this->sdf->Get<std::string>("path");
}

std::cout<<std::endl<<std::endl<<map_path<<std::endl<<std::endl;
	 //TODO: Fix, because i can't read the sdf



}




}

